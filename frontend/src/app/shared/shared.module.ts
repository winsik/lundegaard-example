import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigService } from './config.service';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormControlErrorsComponent } from './form-control-errors/form-control-errors.component';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';

@NgModule({
  declarations: [FormControlErrorsComponent],
  imports: [CommonModule, MessageModule],
  exports: [
    CommonModule,
    NoopAnimationsModule,

    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    InputTextareaModule,
    DropdownModule,
    MessageModule,
    MessagesModule,

    FormControlErrorsComponent
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: (configService: ConfigService) => () => configService.init().toPromise(),
      deps: [ConfigService],
      multi: true
    }
  ]
})
export class SharedModule {
}
