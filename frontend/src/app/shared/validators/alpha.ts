import { AbstractControl, ValidationErrors } from '@angular/forms';

export const alpha = (control: AbstractControl): ValidationErrors | null => {

  const regex = RegExp('^[a-zA-Z]*$');

  if (!control.value || regex.test(control.value)) {
    return null;
  }

  return {
    alpha: true
  };
};
