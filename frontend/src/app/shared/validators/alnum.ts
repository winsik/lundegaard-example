import { AbstractControl, ValidationErrors } from '@angular/forms';

export const alnum = (control: AbstractControl): ValidationErrors | null => {

  const regex = RegExp('^[a-zA-Z0-9]*$');

  if (!control.value || regex.test(control.value)) {
    return null;
  }

  return {
    alnum: true
  };
};
