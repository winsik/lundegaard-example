import { FormControl } from '@angular/forms';
import { alnum } from './alnum';

describe('alnum validator', () => {
  const expectValue = value => expect(alnum(new FormControl(value)));

  it('should not return error when empty', () => {
    expectValue('').toBeNull();
  });

  it('should return error when there is special char', () => {
    expectValue('.').toEqual({ alnum: true });
  });

  it('should not return error for alphanumerical', () => {
    expectValue('abc123').toBeNull();
  });
});
