import { FormControl } from '@angular/forms';
import { alpha } from './alpha';

describe('alpha validator', () => {
  const expectValue = value => expect(alpha(new FormControl(value)));

  it('should not return error when empty', () => {
    expectValue('').toBeNull();
  });

  it('should return error when there is special char', () => {
    expectValue('.').toEqual({alpha: true});
    expectValue('1').toEqual({alpha: true});
    expectValue(' ').toEqual({alpha: true});
  });

  it('should not return error for alpha', () => {
    expectValue('abc').toBeNull();
  });
});
