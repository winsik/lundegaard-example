import { Component, Host, Input, OnInit } from '@angular/core';
import { ControlContainer, FormControl, FormGroupDirective, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'vp-form-control-errors',
  templateUrl: './form-control-errors.component.html'
})
export class FormControlErrorsComponent implements OnInit {

  @Input() controlName: string;

  formControl: FormControl;

  constructor(@Host() private formGroupDirective: FormGroupDirective,
              @Host() private controlContainer: ControlContainer) {
  }

  ngOnInit(): void {
    const controlPath = [...this.controlContainer.path, this.controlName];
    this.formControl = this.formGroupDirective.form.get(controlPath) as FormControl;
  }

  shouldDisplayErrors(): boolean {
    return this.formControl.touched || this.formGroupDirective.submitted;
  }

  get errors(): ValidationErrors {
    return this.formControl.errors;
  }

}
