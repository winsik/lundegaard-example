import { Injectable } from '@angular/core';
import { AppConfig, ConfigControllerService } from '../api';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  config: AppConfig;

  constructor(private configControllerService: ConfigControllerService) {}

  init() {
    return this.configControllerService.getConfig()
      .pipe(tap(config => this.config = config));
  }
}
