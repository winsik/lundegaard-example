export * from './configController.service';
import { ConfigControllerService } from './configController.service';
export * from './requestController.service';
import { RequestControllerService } from './requestController.service';
export const APIS = [ConfigControllerService, RequestControllerService];
