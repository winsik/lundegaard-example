import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RequestComponent } from './request.component';
import { RequestFormComponent } from './request-form/request-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [RequestComponent, RequestFormComponent],
  exports: [
    RequestComponent
  ],
  imports: [
    SharedModule,
  ]
})
export class RequestModule {
}
