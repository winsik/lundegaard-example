import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { CreateRequestDTO, RequestKind } from '../../api';
import { alnum } from '../../shared/validators/alnum';
import { alpha } from '../../shared/validators/alpha';
import { Message } from 'primeng/api';

// tslint:disable-next-line:no-any
export type FormDefinition<T> = { [key in keyof T]: any };

@Component({
  selector: 'vp-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.sass']
})
export class RequestFormComponent implements OnInit {

  @Input() requestKinds: RequestKind[];
  @ViewChild('ngForm') form: FormGroupDirective;

  @Input() set request(value: CreateRequestDTO) {
    this.form?.resetForm();
    this.requestForm.reset(value);
  }

  @Input() loading = false;
  @Input() message: Message = null;
  @Output() requestChange = new EventEmitter<CreateRequestDTO>();

  requestForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {

    const formDefinition: FormDefinition<CreateRequestDTO> = {
      type: [null, Validators.required],
      policyNumber: [null, [Validators.required, alnum]],
      name: [null, [Validators.required, alpha]],
      surname: [null, [Validators.required, alpha]],
      request: [null, [Validators.required, Validators.maxLength(5000)]]
    };

    this.requestForm = this.formBuilder.group(formDefinition);
  }

  ngOnInit(): void {
  }

  submit(requestForm: FormGroup): void {
    this.requestChange.emit({
      ...requestForm.value,
      type: requestForm.value.type.id
    });
  }
}
