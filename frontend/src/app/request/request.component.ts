import { Component } from '@angular/core';
import { ConfigService } from '../shared/config.service';
import { CreateRequestDTO, RequestControllerService } from '../api';
import { Message } from 'primeng/api';

@Component({
  selector: 'vp-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.sass']
})
export class RequestComponent {

  requestKinds = this.configService.config.requestKind;
  request = createEmptyRequest();
  loading = false;
  message: Message;

  constructor(private configService: ConfigService, private requestService: RequestControllerService) {
  }

  sendRequest(request: CreateRequestDTO): void {
    this.loading = true;

    this.requestService.postRequest(request)
      .subscribe(() => {
          this.request = createEmptyRequest();
          this.loading = false;
          this.showOkMessage();
        },
        () => {
          this.showErrorMessage();
        });
  }

  private showErrorMessage(): void {
    this.message = {closable: true, summary: 'Error', detail: 'has odccured when sending request', severity: 'error'};
  }

  private showOkMessage(): void {
    this.message = {closable: true, life: 3000, detail: 'Request has been sent', severity: 'info'};
  }
}

function createEmptyRequest(): CreateRequestDTO {
  return {} as CreateRequestDTO;
}
