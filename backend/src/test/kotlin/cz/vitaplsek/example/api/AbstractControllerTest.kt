package cz.vitaplsek.aukroexample.api


import cz.vitaplsek.example.AbstractDbAwareTest
import cz.vitaplsek.example.api.model.CreateRequestDTO
import cz.vitaplsek.example.jooq.tables.pojos.Request
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
abstract class AbstractControllerTest : AbstractDbAwareTest() {
    @Autowired
    lateinit var restTemplate: TestRestTemplate

    fun getConfig() = getConfigForResponse().body!!
    fun getConfigForResponse() = restTemplate.getForEntity<String>("/config")

    fun getRequests() = getRequestsForResponse().body!!
    fun getRequestsForResponse() = restTemplate.getForEntity<String>("/request")

    fun postRequestResponse(request: CreateRequestDTO) = restTemplate.postForEntity<String>("/request", request)
//
//    fun getSentence(id: Int) = getSentenceResponse(id).body!!
//    fun getSentenceResponse(id: Int) = restTemplate.getForEntity<String>("/sentences/$id")
//
//    fun getYodaSentence(id: Int) = getYodaSentenceResponse(id).body!!
//    fun getYodaSentenceResponse(id: Int) = restTemplate.getForEntity<String>("/sentences/$id/yodaTalk", String::class)
//
//    fun postGenerateSentence() = postGenerateSentenceResponse().body!!
//    fun postGenerateSentenceResponse() = restTemplate.postForEntity<String>("/sentences/generate")
//    fun postGenerateSentenceObject() = restTemplate.postForObject<Sentences>("/sentences/generate")!!
//
//    fun getDuplicateSentences() = getDuplicateSentencesResponse().body!!
//    fun getDuplicateSentencesResponse() = restTemplate.getForEntity<String>("/sentences/duplicates")
//    fun getDuplicateSentencesObject() = restTemplate.getForObject<List<DuplicateSentence>>("/sentences/duplicates")!!
//
//    fun getWordsResponse() = restTemplate.getForEntity<String>("/words")
//    fun getWords() = getWordsResponse().body!!
//
//    fun getWordResponse(word: String) = restTemplate.getForEntity<String>("/words/$word", String::class)
//    fun getWord(word: String) = getWordResponse(word).body!!
//
//    fun postWord(testWord: Words) = restTemplate.postForEntity<String>("/words", testWord)
//    fun postWords(vararg words: Words) = words.forEach { postWord(it) }
}
