package cz.vitaplsek.example.api

import cz.vitaplsek.aukroexample.api.AbstractControllerTest
import cz.vitaplsek.example.jooq.Tables
import cz.vitaplsek.example.jooq.tables.RequestKind
import net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson
import org.junit.jupiter.api.Test


internal class ConfigControllerTest : AbstractControllerTest() {

    @Test
    fun `config should return initial values`() {
        addRequestKindToDb("test1")
        addRequestKindToDb("test2")
        addRequestKindToDb("test3")

        assertThatJson(getConfig())
                .inPath("requestKind[*].description")
                .isArray()
                .hasSize(3)
                .containsExactly("test1", "test2", "test3")
    }
}
