package cz.vitaplsek.example.api

import cz.vitaplsek.aukroexample.api.AbstractControllerTest
import cz.vitaplsek.example.api.model.CreateRequestDTO
import net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.NullSource
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity


internal class RequestControllerTest : AbstractControllerTest() {

    @Test
    fun `there are no requests when app starts`() {

        assertThatJson(getRequests())
                .isArray()
                .isEmpty()
    }

    @Test
    fun `cannot add request for non existing kind `() {

        val response = postRequestResponse{ type = 2 }

        assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThatJson(response.body!!)
                .node("message")
                .isEqualTo("Given request type does not exist: 2");
    }

    @Test
    fun `request can be added into db`() {

        val response = postRequestResponse{
            type = addRequestKindToDb("type")
        }
        assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED);

        assertThatJson(getRequests())
                .isArray()
                .hasSize(1);
    }

    @Nested
    @DisplayName("Validation")
    inner class Validation {

        @DisplayName("Policy number alphanumerical")
        @ParameterizedTest()
        @ValueSource(strings = [".", "$"])
        fun `policy number`(policyNumber: String) {

            val response = postRequestResponse{
                this.policyNumber = policyNumber
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("policyNumber", "Policy number should be alphanumerical")
        }

        @Test
        fun `policy number is required`() {

            val response = postRequestResponse{
                this.policyNumber = ""
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("policyNumber", "Policy number is required")
        }

        @DisplayName("Name lettets")
        @ParameterizedTest()
        @ValueSource(strings = [".", "1"])
        fun `name`(name: String) {

            val response = postRequestResponse{
                this.name = name
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("name", "Name should contain only letters")
        }

        @Test
        fun `name is required`() {

            val response = postRequestResponse{
                this.name = ""
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("name", "Name is required")
        }

        @DisplayName("Surname lettets")
        @ParameterizedTest()
        @ValueSource(strings = [".", "1"])
        fun `surname`(surname: String) {

            val response = postRequestResponse{
                this.surname = surname
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("surname", "Surname should contain only letters")
        }

        @Test
        fun `surname is required`() {

            val response = postRequestResponse{ this.surname = "" }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("surname", "Surname is required")
        }

        @Test
        fun `request is required`() {

            val response = postRequestResponse{
                this.request = ""
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("request", "Request is required")
        }

        @Test
        fun `request can have 5000 char`() {

            val response = postRequestResponse{
                this.request = "X".repeat(5000)
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED);
        }

        @Test
        fun `request cannot be longer then 5000`() {

            val response = postRequestResponse{
                this.request = "X".repeat(5001)
            }

            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST);
            response.assertError("request", "Request has to be shorter then 5000 chars")
        }
    }

    fun createRequest(apply: CreateRequestDTO.() -> Unit) =
            CreateRequestDTO(
                    type = addRequestKindToDb("type"),
                    policyNumber = "policyNumber",
                    name = "name",
                    surname = "surname",
                    request = "request text")
                    .apply(apply)

    fun postRequestResponse(apply: CreateRequestDTO.() -> Unit) = postRequestResponse(createRequest(apply))
}

private fun <T> ResponseEntity<T>.assertError(field: String, expectedMessage: String) {
    assertThatJson(this.body!!)
            .node("errors.[0].field")
            .isEqualTo(field)

    assertThatJson(this.body!!)
            .node("errors.[0].defaultMessage")
            .isEqualTo(expectedMessage);
}
