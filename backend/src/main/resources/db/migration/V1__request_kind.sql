create table request_kind
(
    id          int primary key auto_increment,
    description varchar
);

insert into request_kind (description)
values ('Contract adjustment'),
       ('Damage Case'),
       ('Complaint');

create table request
(
    id            long primary key auto_increment,
    type          int,
    policy_number varchar,
    name          varchar,
    surname       varchar,
    request       text,

    constraint request_kind__fk
        foreign key (type) references request_kind (ID)
);

