package cz.vitaplsek.example.api.model

import cz.vitaplsek.example.jooq.tables.pojos.RequestKind

data class AppConfig (var requestKind: List<RequestKind>);
