package cz.vitaplsek.example.api.model

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern

data class CreateRequestDTO(
        var type: Int,

        @field: NotEmpty( message = "Policy number is required")
        @field: Pattern(  regexp = "^\\p{Alnum}*$", message = "Policy number should be alphanumerical")
        var policyNumber: String,

        @field: NotEmpty( message = "Name is required")
        @field: Pattern(  regexp = "^\\p{Alpha}*$", message = "Name should contain only letters")
        var name: String,

        @field: NotEmpty( message = "Surname is required")
        @field: Pattern(  regexp = "^\\p{Alpha}*$", message = "Surname should contain only letters")
        var surname: String,

        @field: NotEmpty( message = "Request is required")
        @field: Length( max = 5000, message = "Request has to be shorter then 5000 chars")
        var request: String)
