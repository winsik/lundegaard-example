package cz.vitaplsek.example.api

import cz.vitaplsek.example.api.model.AppConfig
import cz.vitaplsek.example.domain.RequestKindService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController()
class ConfigController(val requestKindService: RequestKindService) {

    @GetMapping("config")
    fun getConfig() = AppConfig(requestKind = requestKindService.findAll());
}
