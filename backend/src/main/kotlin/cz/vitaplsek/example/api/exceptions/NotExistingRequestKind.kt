package cz.vitaplsek.example.api.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class NotExistingRequestKind(requestKindId: Int) :
        ResponseStatusException(HttpStatus.BAD_REQUEST, "Given request type does not exist: $requestKindId")
