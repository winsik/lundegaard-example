package cz.vitaplsek.example.api

import cz.vitaplsek.example.api.model.CreateRequestDTO
import cz.vitaplsek.example.domain.RequestService
import cz.vitaplsek.example.jooq.tables.pojos.Request
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController()
class RequestController(val requestService: RequestService) {

    @GetMapping("request")
    fun getRequests() = requestService.findAll();

    @PostMapping("request")
    @ResponseStatus(HttpStatus.CREATED)
    fun postRequest(@Valid @RequestBody request: CreateRequestDTO) = requestService.insertRequest(request);
}
