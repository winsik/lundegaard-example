package cz.vitaplsek.example.domain

import cz.vitaplsek.example.api.exceptions.NotExistingRequestKind
import cz.vitaplsek.example.api.model.CreateRequestDTO
import cz.vitaplsek.example.jooq.tables.daos.RequestDao
import cz.vitaplsek.example.jooq.tables.pojos.Request
import org.springframework.beans.BeanUtils
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Component

@Component
class RequestService(configuration: org.jooq.Configuration, val requestKindService: RequestKindService) : RequestDao(configuration) {

    fun insertRequest(createRequest: CreateRequestDTO) {

        try {

            return super.insert(createRequest.toRequest())
        } catch (exception: DataIntegrityViolationException) {
            throw NotExistingRequestKind(createRequest.type)
        }
    }
}

private fun CreateRequestDTO.toRequest(): Request {
    val request = Request();
    BeanUtils.copyProperties(this, request);

    return request;
}
