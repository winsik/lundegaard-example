package cz.vitaplsek.example.domain

import cz.vitaplsek.example.jooq.tables.daos.RequestDao
import cz.vitaplsek.example.jooq.tables.daos.RequestKindDao
import org.springframework.stereotype.Component

@Component
class RequestKindService(configuration: org.jooq.Configuration) : RequestKindDao(configuration) {

}
