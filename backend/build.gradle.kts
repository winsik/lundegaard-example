import com.rohanprabhu.gradle.plugins.kdjooq.*
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    dependencies {
        classpath("com.h2database:h2:1.4.200")
    }

    repositories {
        mavenCentral()
        maven {
            setUrl("https://plugins.gradle.org/m2/")
        }
    }
}

plugins {
    id("org.springframework.boot") version "2.2.6.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    id("com.rohanprabhu.kotlin-dsl-jooq") version "0.4.5"
    id("org.flywaydb.flyway") version "6.4.0"
    kotlin("jvm") version "1.3.71"
    kotlin("plugin.spring") version "1.3.71"
}

group = "cz.vitaplsek.example"

version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-jooq")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.github.microutils:kotlin-logging:1.7.9")
    implementation("org.flywaydb:flyway-core")

    implementation("org.springdoc:springdoc-openapi-ui:1.4.8")
    implementation("org.springdoc:springdoc-openapi-kotlin:1.4.8")

    runtimeOnly("com.h2database:h2:1.4.200")
    jooqGeneratorRuntime("com.h2database:h2:1.4.200")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }

    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("net.javacrumbs.json-unit:json-unit-assertj:2.17.0")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

val h2DBUrl = "jdbc:h2:${project.buildDir}/tmp/h2/jooq"

flyway {
    url = h2DBUrl
    user = "sa"
}

jooqGenerator {
    configuration("primary", project.sourceSets.getByName("main")) {
        configuration = jooqCodegenConfiguration {
            jdbc {
                username = "sa"
                driver = "org.h2.Driver"
                url = h2DBUrl
            }

            generator {
                target {
                    packageName = "cz.vitaplsek.example.jooq"
                    directory = "src/main/generated/java"
                }

                database {
                    name = "org.jooq.meta.h2.H2Database"
                    inputSchema = "PUBLIC"
                    excludes = "flyway_schema_history"
                }

                generate {
                    withRelations(true)
                    withDeprecated(false)
                    withRecords(true)
                    withFluentSetters(false)
                    withDaos(true)
                    withPojosEqualsAndHashCode(true)
                    withGeneratedAnnotation(true)
                }
            }
        }
    }
}

val `jooq-codegen-primary` by project.tasks
`jooq-codegen-primary`.dependsOn("flywayMigrate")

val compileKotlin: KotlinCompile by tasks
compileKotlin.dependsOn("jooq-codegen-primary")
