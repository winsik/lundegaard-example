## Lundegaard example

### Technologie
 * backend - spring boot, gradle, kotlin, jooq, h2, swagger pro generování modelu
 * frontend - angular, primeng

### Spuštění
 Požadavek na spuštění jedním příkazem jsem vzal doslovně a udělal i build v dockeru

```
docker-compose up
```

Na `localhost` běží FE a na `localhost:8080` BE

image jsou postaveny tak, že i sbuidlují aplikaci. v reálu bych to dělal spíš na CI a dockerfile jen aplikaci zabalil. Ale tady pro ten účel to funguje docela hezky.  

### Popis řešení a možných vylepšení

Backend má api pokryto testy. Stálo by zato ještě udělat build jako multilayer pro lepší dockerování.

Docker sám by zde snesl optimalizaci a právě spojení s CI kde by se sdílely stažené artefakty, tohle řešení jsem udělal abych zkusil, že to jde zkompilovat i takto. 

Frontend má v sobě vygenerované API pro komunikaci se serverem ze swaggeru a task pro přegenerování, v reálu bych api buildoval na CI a přidával do npm registry.
Použil jsem pro ui PrimeNg, které jsem nezanl abych si ho zkusil, a výsledek není graficky uplně super, ale stačí. Zde bych u větší aplikace použil pro řízení stavu ngrx, tady mi to přišlo v pohodě takto.

Validace jsou jak na BE (pokryto testy), tak na FE. 
Při uložení nebo chybě (možno nasimulovat nedostupností serveru) se vypíše hláška. 

## Ukázka
Video: https://youtu.be/ItDbXoi2qbM
